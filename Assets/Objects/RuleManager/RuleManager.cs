﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuleManager : MonoBehaviour {

    GameManager gameManager;

    public Vector2Int boardWidthRange = new Vector2Int(3, 5);
    public Vector2Int boardHeightRange = new Vector2Int(3, 5);

    public int MaxPieces { get; private set;  }
    public Vector2Int BoardDimensions { get; private set; }

    public Piece[] pieces { get; private set; }

	// Use this for initialization
	void Start () {

        gameManager = GameManager.GetInstance();

        ReRoll();
        pieces = Resources.LoadAll<Piece>("PiecesPrefabs");

        gameManager.CreateUIPieces(pieces);

        gameManager.EquippedPiece = pieces[0];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ReRoll()
    {
        MaxPieces = Random.Range(2, 5);
        BoardDimensions = new Vector2Int(Random.Range(boardWidthRange.x, boardWidthRange.y), Random.Range(boardHeightRange.x, boardHeightRange.y));
    }

    public static RuleManager GetInstance()
    {
        return (RuleManager) FindObjectOfType(typeof(RuleManager));
    }

}
