﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

    public BoardCell boardCell;

    private BoardCell[,] cells;
    private SpriteRenderer boardCellArea;

    RuleManager ruleManager;

    public float GridSpacing = 1.1f;

    int piecesPlaced = 0;

	// Use this for initialization
	void Start () {
        ruleManager = RuleManager.GetInstance();
        boardCellArea = GetComponentInChildren<SpriteRenderer>();
        GenerateBoard(ruleManager.BoardDimensions.x, ruleManager.BoardDimensions.y);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("RegenerateBoard"))
        {
            GetNewBoard();
        }
	}

    void GetNewBoard()
    {
        DeleteBoard();
        ruleManager.ReRoll();
        GenerateBoard(ruleManager.BoardDimensions.x, ruleManager.BoardDimensions.y);
    }

    void GenerateBoard(int width, int height)
    {
        cells = new BoardCell[width,height];

        float halfWidth = width / 2;
        float halfHeight = height / 2;

        if (width % 2 == 0)
            halfWidth -= 0.5f;

        if (height % 2 == 0)
            halfHeight -= 0.5f;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                cells[x, y] = Instantiate(boardCell, new Vector2((x*GridSpacing) - halfWidth, (y*GridSpacing) - halfHeight), Quaternion.identity, transform);
            }
        }
    }

    void DeleteBoard()
    {
        foreach (BoardCell cell in cells)
        {
            DeleteCell(cell);
        }
    }

    void DeleteCell(BoardCell cell)
    {
        if (cell.ContainsPiece)
        {
            piecesPlaced--;
            Debug.Log(piecesPlaced);
        }
        Destroy(cell.gameObject);
    }

    public bool hasPiecesRemaining()
    {
        return piecesPlaced < ruleManager.MaxPieces;
    }

    public void IncrementPieceCount()
    {
        piecesPlaced++;    
    }
}
