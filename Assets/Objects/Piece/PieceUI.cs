﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceUI : MonoBehaviour {

    GameManager gameManager;

    public Piece Piece { get; set; }

	// Use this for initialization
	void Start () {
        gameManager = GameManager.GetInstance();
	}
	
	// Update is called once per frame
	void Update () {

        //Touch[] touches = Input.touches;

        //touches[0].
	}

    private void OnMouseDown()
    {
        gameManager.EquippedPiece = Piece;
    }

}
