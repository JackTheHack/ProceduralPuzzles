﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{

    RuleManager ruleManager;

    PieceColour colour;

    SpriteRenderer sprite;

    // Use this for initialization
    void Start()
    {
        ruleManager = RuleManager.GetInstance();
        colour = GetRandomPieceColour();
        sprite = GetComponent<SpriteRenderer>();

        sprite.color = PieceColourToSpriteColour(colour);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public enum PieceColour { Red, Green, Blue, Orange }

    PieceColour GetRandomPieceColour()
    {
        var enumValue = System.Enum.GetValues(typeof(PieceColour));
        return (PieceColour) enumValue.GetValue(new System.Random().Next(enumValue.Length));
    }

    Color PieceColourToSpriteColour(PieceColour colour)
    {
        switch (colour)
        {
            case PieceColour.Red:
                return new Color(1f, 0f, 0f);
            case PieceColour.Green:
                return new Color(0f, 1f, 0f);
            case PieceColour.Blue:
                return new Color(0f, 0f, 1f);
            case PieceColour.Orange:
                return new Color(1f, 0.55f, 0f);
        }
        return new Color(1f, 1f, 1f);
    }
}
