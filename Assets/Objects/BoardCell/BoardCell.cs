﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardCell : MonoBehaviour {
    
    GameManager gameManager;
    BoardManager boardManager;

    Piece piece;

    public bool ContainsPiece { get; private set; }
    public bool IsHole { get; private set; }

	// Use this for initialization
	void Start () {
        boardManager = GetComponentInParent<BoardManager>();
        gameManager = GameManager.GetInstance();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseDown()
    {
        if (isValidToPlacePiece())
        {
            PlacePiece();
        }
    }

    bool isValidToPlacePiece()
    {
        if (ContainsPiece)
        {
            Debug.Log("Can't place piece here! Reason: Cell already contains a piece.");
            return false;
        }
        else if (IsHole)
        {
            Debug.Log("Can't place piece here! Reason: Cell is a hole.");
            return false;
        }
        else if (!boardManager.hasPiecesRemaining())
        {
            Debug.Log("Can't place piece here! Reason: No pieces remaining.");
            return false;
        }
        return true;
    }

    void PlacePiece()
    {
        piece = Instantiate(gameManager.EquippedPiece, transform.position, Quaternion.identity, transform);
        boardManager.IncrementPieceCount();
        ContainsPiece = true;
    }
}
