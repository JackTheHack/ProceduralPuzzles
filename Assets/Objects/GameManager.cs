﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public PieceUI PieceUIPrefab;
    public Piece EquippedPiece { get; set; }

    public PieceUI[] pieceSelectorIcons { get; private set; }

	// Use this for initialization
	void Start () {
		

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CreateUIPieces(Piece[] pieces)
    {
        pieceSelectorIcons = new PieceUI[pieces.Length];

        for (int i = 0; i < pieces.Length; i++)
        {
            // PieceUI instances have copy of relevant piece instance so the 
            // piece can be selected when they are clicked by the user.
            PieceUI pieceUI = Instantiate(PieceUIPrefab, new Vector2(i * 2, -5f), Quaternion.identity);
            pieceUI.GetComponent<SpriteRenderer>().sprite = pieces[i].GetComponent<SpriteRenderer>().sprite;
            pieceUI.Piece = pieces[i];
            pieceSelectorIcons[i] = pieceUI;    
        }

    }

    public static GameManager GetInstance()
    {
        return (GameManager) FindObjectOfType(typeof(GameManager));
    }
}
